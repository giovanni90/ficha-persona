import { LitElement, html } from 'lit-element'; 
	import '../persona-ficha-listado/persona-ficha-listado.js';  
    import '../persona-form/persona-form.js';
	class PersonaMain extends LitElement {		 
		static get properties() {
			return {			
				people: {type: Array},
                showPersonForm: {type: Boolean}
			};
		}	

		constructor() {
			super();
			this.showPersonForm = false;
			this.people = [
			{
				name: "Ellen Ripley",
				yearsInCompany: 10,
				profile: "Lorem ipsum dolor sit amet.",
				photo: {
					"src": "./img/persona.png",
					"alt": "Ellen Ripley"
			},
				canTeach: false				
			}, {
				name: "Bruce Banner",		
				yearsInCompany: 2,
				profile: "Lorem ipsum.",
				photo: {
					"src": "./img/persona.png",
					"alt": "Bruce Banner"
				},
				canTeach: true
			}, {
				name: "Éowyn",
				yearsInCompany: 5,
				profile: "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
				photo: {
					"src": "./img/persona.png",
					"alt": "Éowyn"
				},
				canTeach: true
			}, {
				name: "Turanga Leela",
				yearsInCompany: 9,
				profile: "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod.",
				photo: {
					"src": "./img/persona.png",
					"alt": "Turanga Leela"
				},
				canTeach: true
			}, {
				name: "Tyrion Lannister",
				yearsInCompany: 1,
				profile: "Lorem ipsum.",
				photo: {
					"src": "./img/persona.png",
					"alt": "Tyrion Lannister"
				},
				canTeach: false
			}
		];							
	}
		
	render() {
		return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">	
			<h2 class="text-center">Personas</h2>
			<div class="row" id="peopleList">		
				<div class="row row-cols-1 row-cols-sm-4">
					${this.people.map(
						person => 
						html`<persona-ficha-listado 
							name="${person.name}" 
							yearsInCompany="${person.yearsInCompany}" 
							profile="${person.profile}" 
							.photo="${person.photo}"
							@delete-person="${this.deletePerson}">
						</persona-ficha-listado>`
				)}
			</div>
		</div>
        <div class="row">
            <persona-form id="personForm" class="d-none border rounded border-primary"
            @persona-form-close="${this.personFormClose}"
            @persona-form-store="${this.personFormStore}" >
        </persona-form>
        </div>					
		`;
	}

    personFormStore(e) {
        console.log("personFormStore");
        console.log("Se va a almacenar una persona");	
                            
        this.people.push(e.detail.person);
      
        console.log("Persona almacenada");	
        this.showPersonForm = false;
    }

	personFormClose() {
		console.log("personFormClose");
		console.log("Se ha cerrado el formulario de la persona");
	  
		this.showPersonForm = false;	
	}

    updated(changedProperties) { 
        console.log("updated");	
        if (changedProperties.has("showPersonForm")) {
            console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");
            if (this.showPersonForm === true) {
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }
    }

    showPersonList() {
        console.log("showPersonList");
        console.log("Mostrando listado de personas");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");	
        }  
    showPersonFormData() {
        console.log("showPersonFormData");
        console.log("Mostrando formulario de persona");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");	  
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");	
    } 

	deletePerson(e) {
		console.log("deletePerson en persona-main");
		console.log("Se va a borrar la persona de nombre " + e.detail.name);
	
		this.people = this.people.filter(
			person => person.name != e.detail.name
		);
	}
}  
customElements.define('persona-main', PersonaMain);