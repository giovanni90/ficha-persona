import { LitElement, html,css } from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer';
import '../persona-sidebar/persona-sidebar.js'; 

class PersonaApp extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    static get styles(){
        return css `
          .row {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: flex-start;
            color: #1a2b42;
            min-width: 200px;
            margin: 0 auto;
            text-align: center;
          }
          .item-center {
            text-align: center; 
          }`;
    } 
    render() {
        return html`
            <persona-header class="item-center"></persona-header>
            <div class="row">
                <persona-sidebar class="col-2" @new-person="${this.newPerson}"></persona-sidebar>
                <persona-main class="col-10"></persona-main>
            </div>			
            <persona-footer class="item-center"></persona-footer>
        `;
    }

    newPerson(e) {
		console.log("newPerson en PersonaApp");	
		this.shadowRoot.querySelector("persona-main").showPersonForm = true; 	  	
	}
}

customElements.define('persona-app', PersonaApp)