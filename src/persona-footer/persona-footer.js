import { LitElement, html } from 'lit-element';

class PersonaFooter extends LitElement {

    static get properties() {
        return {
            year: { type: Number }
        };
    }

    constructor() {
        super();
        const date = new Date();
        this.year = date.getFullYear();
    }

    render() {
        return html`
            <h5>@Persona App ${this.year}</h5>
        `;
    }
}

customElements.define('persona-footer', PersonaFooter)